#ifndef SERIALCOMMUNICATION_H
#define SERIALCOMMUNICATION_H

#include <QtSerialPort/QSerialPort>
#include <QTimer>
#include <QTextEdit>

class SerialCommunication: public QObject
{
    Q_OBJECT

private:


public:

    QStringList discoverComPorts();

    SerialCommunication(QObject *parent = 0);

public slots:

    QSerialPort *startCom(QString portName, int baudrate);

};


#endif // SERIALCOMMUNICATION_H


