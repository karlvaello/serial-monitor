#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "serialcommunication.h"
#include <QDebug>
#include <QTimer>
#include <QObject>
#include <QtSerialPort/QSerialPort>

/**
    Default constructor with some start setup
*/
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    serialPort = new SerialCommunication(this);
    availablePortList = new QStringList( serialPort->discoverComPorts());
    connectionStatus = false;

    discoverPorts();
}

/**
    Default destructor
*/
MainWindow::~MainWindow(){
    delete ui;
}

/**
    Encodes a single digit of a POSTNET "A" bar code.
    @param digit the single digit to encode.
    @return void
*/
void MainWindow::on_setStatus_pushButton_clicked(){
    if(connectionStatus == false){
        portOnConnect = serialPort->startCom(ui->selectedPort_comboBox->currentText(),ui->selectedBaudrate_comboBox->currentText().toInt());

        if(portOnConnect->isOpen()) {
            portOnConnect->clear();
            connectionStatus = true;
            timer = new QTimer();
            timer->start(20); //20ns
            connect(timer,SIGNAL(timeout()),this,SLOT(readData()));
            ui->conState_label->setText("CONNECTED");
            ui->portConnected_label->setText(ui->selectedPort_comboBox->currentText());
            ui->bdSelected_label->setText(ui->selectedBaudrate_comboBox->currentText());
            ui->writeData_pushButton->setEnabled(true);
            ui->writeData_lineEdit->setEnabled(true);
            ui->autoscroll_checkBox->setEnabled(true);
            ui->setStatus_pushButton->setText("Disconnect");
        }else {
            qDebug() << "ERROR: communication not working";
            QString errorComMsg;
            errorComMsg = "COMMUNICATION ERROR ON PORT " + ui->selectedPort_comboBox->currentText() + " @ " + ui->selectedBaudrate_comboBox->currentText() +"";

            ui->recivedDataTextEdit->append(errorComMsg);

        }
    }else{
        portOnConnect->close();
        timer->stop();
        connectionStatus = false;
        ui->writeData_pushButton->setEnabled(false);
        ui->writeData_lineEdit->setEnabled(false);
        ui->autoscroll_checkBox->setEnabled(false);
        ui->setStatus_pushButton->setText("Connect");
        if(ui->cleanmon_checkBox->checkState() == Qt::Checked){
            ui->recivedDataTextEdit->clear();
        }
    }
}

/**
    Action when discoverPorts_pushButton clicked.
    Refresh discovered ports
*/
void MainWindow::on_discoverPorts_pushButton_clicked() {
    discoverPorts();
}

/**
    Action when discoverPorts_pushButton clicked.
    Refresh discovered ports calling discoverPorts() function
*/
void MainWindow::on_writeData_pushButton_clicked() {
    QByteArray buffer;

    buffer.append(ui->writeData_lineEdit->text());
    writeData(buffer);
    ui->writeData_lineEdit->clear();
}

/**
    Read incoming serial data from open port
    and print it on main window
*/
void MainWindow::readData(){
    incomingSerialData = portOnConnect->readLine();
    dBuffer = QString::fromStdString(incomingSerialData.toStdString()).simplified();
    qDebug() << incomingSerialData;
    if(incomingSerialData != NULL){
        ui->recivedDataTextEdit->append("MCU-> " + incomingSerialData.simplified());
        incomingSerialData = NULL;
    }
}

/**
    Transmit data over serial port
    @param data - data to be transmited (QByteArray)
    @return a bar code of the digit using "|" as the long
    bar and "," as the half bar.
*/
void MainWindow::writeData(const QByteArray &data)
{
    portOnConnect->write(data + "\r");
    ui->recivedDataTextEdit->append("> " + data);
    qDebug() << data << "\r";
}

/**
    Action when writeData_lineEdit detect return pressed.
    Refresh discovered ports
*/
void MainWindow::on_writeData_lineEdit_returnPressed(){

    QByteArray buffer;

    buffer.append(ui->writeData_lineEdit->text());

    writeData(buffer);
    ui->writeData_lineEdit->clear();

    //buffer = "";
}

/**
    Discover new ports routine
*/
void MainWindow::discoverPorts(){
    availablePortList->clear();
    availablePortList = new QStringList( serialPort->discoverComPorts());
    ui->selectedPort_comboBox->clear();
    for(int i = 0; i < availablePortList->size(); i++){
       ui->selectedPort_comboBox->addItem(availablePortList->at(i));
    }
}
