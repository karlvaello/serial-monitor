/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLineEdit *writeData_lineEdit;
    QGroupBox *groupBox;
    QPushButton *setStatus_pushButton;
    QLabel *label_2;
    QLabel *label_3;
    QComboBox *selectedPort_comboBox;
    QPushButton *discoverPorts_pushButton;
    QLabel *currentState_label;
    QLabel *currentPort_label;
    QLabel *currentBaudrate_label;
    QLabel *portConnected_label;
    QLabel *bdSelected_label;
    QLabel *conState_label;
    QComboBox *selectedBaudrate_comboBox;
    QCheckBox *cleanmon_checkBox;
    QTextEdit *recivedDataTextEdit;
    QCheckBox *autoscroll_checkBox;
    QPushButton *writeData_pushButton;
    QLabel *version_label;
    QLabel *creator_label;
    QGroupBox *groupBox_2;
    QLabel *label_4;
    QComboBox *selectedNL_RX_comboBox;
    QLabel *label_5;
    QComboBox *selectedNL_TX_comboBox;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1050, 540);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(1050, 540));
        MainWindow->setMaximumSize(QSize(1050, 540));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        writeData_lineEdit = new QLineEdit(centralWidget);
        writeData_lineEdit->setObjectName(QStringLiteral("writeData_lineEdit"));
        writeData_lineEdit->setEnabled(false);
        writeData_lineEdit->setGeometry(QRect(130, 510, 581, 24));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(800, 10, 231, 381));
        setStatus_pushButton = new QPushButton(groupBox);
        setStatus_pushButton->setObjectName(QStringLiteral("setStatus_pushButton"));
        setStatus_pushButton->setGeometry(QRect(10, 330, 211, 41));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(11, 30, 55, 16));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(11, 130, 55, 20));
        selectedPort_comboBox = new QComboBox(groupBox);
        selectedPort_comboBox->setObjectName(QStringLiteral("selectedPort_comboBox"));
        selectedPort_comboBox->setGeometry(QRect(10, 50, 211, 24));
        discoverPorts_pushButton = new QPushButton(groupBox);
        discoverPorts_pushButton->setObjectName(QStringLiteral("discoverPorts_pushButton"));
        discoverPorts_pushButton->setGeometry(QRect(10, 76, 211, 25));
        currentState_label = new QLabel(groupBox);
        currentState_label->setObjectName(QStringLiteral("currentState_label"));
        currentState_label->setGeometry(QRect(10, 210, 91, 16));
        currentPort_label = new QLabel(groupBox);
        currentPort_label->setObjectName(QStringLiteral("currentPort_label"));
        currentPort_label->setGeometry(QRect(41, 235, 55, 16));
        currentPort_label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        currentBaudrate_label = new QLabel(groupBox);
        currentBaudrate_label->setObjectName(QStringLiteral("currentBaudrate_label"));
        currentBaudrate_label->setGeometry(QRect(41, 260, 55, 16));
        currentBaudrate_label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        portConnected_label = new QLabel(groupBox);
        portConnected_label->setObjectName(QStringLiteral("portConnected_label"));
        portConnected_label->setGeometry(QRect(100, 235, 55, 16));
        bdSelected_label = new QLabel(groupBox);
        bdSelected_label->setObjectName(QStringLiteral("bdSelected_label"));
        bdSelected_label->setGeometry(QRect(100, 260, 55, 16));
        conState_label = new QLabel(groupBox);
        conState_label->setObjectName(QStringLiteral("conState_label"));
        conState_label->setGeometry(QRect(100, 210, 91, 16));
        selectedBaudrate_comboBox = new QComboBox(groupBox);
        selectedBaudrate_comboBox->setObjectName(QStringLiteral("selectedBaudrate_comboBox"));
        selectedBaudrate_comboBox->setGeometry(QRect(10, 150, 211, 24));
        cleanmon_checkBox = new QCheckBox(groupBox);
        cleanmon_checkBox->setObjectName(QStringLiteral("cleanmon_checkBox"));
        cleanmon_checkBox->setEnabled(true);
        cleanmon_checkBox->setGeometry(QRect(10, 300, 191, 22));
        cleanmon_checkBox->setChecked(true);
        cleanmon_checkBox->setTristate(false);
        recivedDataTextEdit = new QTextEdit(centralWidget);
        recivedDataTextEdit->setObjectName(QStringLiteral("recivedDataTextEdit"));
        recivedDataTextEdit->setGeometry(QRect(10, 20, 781, 481));
        autoscroll_checkBox = new QCheckBox(centralWidget);
        autoscroll_checkBox->setObjectName(QStringLiteral("autoscroll_checkBox"));
        autoscroll_checkBox->setEnabled(false);
        autoscroll_checkBox->setGeometry(QRect(10, 510, 83, 22));
        writeData_pushButton = new QPushButton(centralWidget);
        writeData_pushButton->setObjectName(QStringLiteral("writeData_pushButton"));
        writeData_pushButton->setEnabled(false);
        writeData_pushButton->setGeometry(QRect(720, 510, 71, 25));
        version_label = new QLabel(centralWidget);
        version_label->setObjectName(QStringLiteral("version_label"));
        version_label->setGeometry(QRect(954, 510, 81, 20));
        version_label->setLayoutDirection(Qt::LeftToRight);
        version_label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        creator_label = new QLabel(centralWidget);
        creator_label->setObjectName(QStringLiteral("creator_label"));
        creator_label->setGeometry(QRect(890, 513, 55, 16));
        creator_label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(800, 400, 231, 101));
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(10, 33, 55, 16));
        label_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        selectedNL_RX_comboBox = new QComboBox(groupBox_2);
        selectedNL_RX_comboBox->setObjectName(QStringLiteral("selectedNL_RX_comboBox"));
        selectedNL_RX_comboBox->setGeometry(QRect(70, 30, 141, 24));
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(10, 73, 55, 16));
        label_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        selectedNL_TX_comboBox = new QComboBox(groupBox_2);
        selectedNL_TX_comboBox->setObjectName(QStringLiteral("selectedNL_TX_comboBox"));
        selectedNL_TX_comboBox->setGeometry(QRect(70, 70, 141, 24));
        selectedNL_TX_comboBox->setEditable(false);
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        selectedBaudrate_comboBox->setCurrentIndex(7);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Serial Monitor", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("MainWindow", "Settings", Q_NULLPTR));
        setStatus_pushButton->setText(QApplication::translate("MainWindow", "Connect", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "Port", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "Baudrate", Q_NULLPTR));
        discoverPorts_pushButton->setText(QApplication::translate("MainWindow", "Discover Ports", Q_NULLPTR));
        currentState_label->setText(QApplication::translate("MainWindow", "Currente state:", Q_NULLPTR));
        currentPort_label->setText(QApplication::translate("MainWindow", "Port:", Q_NULLPTR));
        currentBaudrate_label->setText(QApplication::translate("MainWindow", "Baudrate:", Q_NULLPTR));
        portConnected_label->setText(QApplication::translate("MainWindow", "NONE", Q_NULLPTR));
        bdSelected_label->setText(QApplication::translate("MainWindow", "NONE", Q_NULLPTR));
        conState_label->setText(QApplication::translate("MainWindow", "DISCONNECTED", Q_NULLPTR));
        selectedBaudrate_comboBox->clear();
        selectedBaudrate_comboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "9600", Q_NULLPTR)
         << QApplication::translate("MainWindow", "14400", Q_NULLPTR)
         << QApplication::translate("MainWindow", "19200", Q_NULLPTR)
         << QApplication::translate("MainWindow", "28800", Q_NULLPTR)
         << QApplication::translate("MainWindow", "31250", Q_NULLPTR)
         << QApplication::translate("MainWindow", "38400", Q_NULLPTR)
         << QApplication::translate("MainWindow", "57600", Q_NULLPTR)
         << QApplication::translate("MainWindow", "115200", Q_NULLPTR)
         << QApplication::translate("MainWindow", "225000", Q_NULLPTR)
        );
        cleanmon_checkBox->setText(QApplication::translate("MainWindow", "Clean monitor on reconnect", Q_NULLPTR));
        autoscroll_checkBox->setText(QApplication::translate("MainWindow", "Autoscroll", Q_NULLPTR));
        writeData_pushButton->setText(QApplication::translate("MainWindow", "Send", Q_NULLPTR));
        version_label->setText(QApplication::translate("MainWindow", "Carlos Vaello", Q_NULLPTR));
        creator_label->setText(QApplication::translate("MainWindow", "v1.2.0", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "New-line", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "Receive", Q_NULLPTR));
        selectedNL_RX_comboBox->clear();
        selectedNL_RX_comboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "CR (/n)", Q_NULLPTR)
         << QApplication::translate("MainWindow", "CR+LF(/n/r)", Q_NULLPTR)
         << QApplication::translate("MainWindow", "LF (/r)", Q_NULLPTR)
         << QApplication::translate("MainWindow", "AUTO", Q_NULLPTR)
        );
        label_5->setText(QApplication::translate("MainWindow", "Transmit", Q_NULLPTR));
        selectedNL_TX_comboBox->clear();
        selectedNL_TX_comboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "CR (/n)", Q_NULLPTR)
         << QApplication::translate("MainWindow", "CR+LF (/n/r)", Q_NULLPTR)
         << QApplication::translate("MainWindow", "LF (/r)", Q_NULLPTR)
         << QApplication::translate("MainWindow", "AUTO", Q_NULLPTR)
        );
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
