#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "serialcommunication.h"
#include <QtSerialPort/QSerialPort>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    SerialCommunication *serialPort;
    QStringList *availablePortList;
    QTimer *timer;

    Ui::MainWindow *ui;

    QSerialPort *portOnConnect;

private slots:
    void on_setStatus_pushButton_clicked();
    void on_discoverPorts_pushButton_clicked();
    void on_writeData_pushButton_clicked();
    void on_writeData_lineEdit_returnPressed();

    void readData();
    void writeData(const QByteArray &data);

private:
    QByteArray incomingSerialData;
    QString dBuffer;
    QStringList data;
    bool connectionStatus;
    void discoverPorts();

};
#endif // MAINWINDOW_H
