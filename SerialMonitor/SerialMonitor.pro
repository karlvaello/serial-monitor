#-------------------------------------------------
#
# Project created by QtCreator 2018-10-13T13:25:47
#
#-------------------------------------------------

QT += widgets core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SerialMonitor
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    serialcommunication.cpp

HEADERS += \
        mainwindow.h \
    serialcommunication.h

FORMS += \
        mainwindow.ui
