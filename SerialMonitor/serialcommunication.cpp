#include "serialcommunication.h"
#include <QtSerialPort/qserialport.h>
#include <QtSerialPort/qserialportinfo.h>
#include <QDebug>
#include <QThread>
#include <QElapsedTimer>
#include <QObject>
#include <QTextEdit>

/**
    Default constructor
*/
SerialCommunication::SerialCommunication(QObject *parent): QObject(parent){}

/**
    Discover available ports and retourn it on a QStringList
    @return a QStringList that contains available ports
*/
QStringList SerialCommunication::discoverComPorts(){

    QStringList portList;

    //qDebug() << "Number of ports: " << QSerialPortInfo::availablePorts().length() << "\n";
    foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts()){
        portList.append(serialPortInfo.portName());
        /*
            qDebug() << "Port Name: " << serialPortInfo.portName() << "\n";
            qDebug() << "Description: " << serialPortInfo.description() << "\n";
            qDebug() << "Has vendor id?: " << serialPortInfo.hasVendorIdentifier() << "\n";
            qDebug() << "Vendor ID: " << serialPortInfo.vendorIdentifier() << "\n";
            qDebug() << "Has product id?: " << serialPortInfo.hasProductIdentifier() << "\n";
            qDebug() << "Product ID: " << serialPortInfo.productIdentifier() << "\n";
        */
    }
    return portList;
}


/**
    Open and connect to port and return the port (QSerialPort)
    @param portName (QString) - port name to connect
    @param baudrate (int) - baudarete speet
    @return a QStringList that contains available ports
*/
QSerialPort * SerialCommunication::startCom(QString portName, int baudrate){
    QSerialPort *comPort = new QSerialPort();
    comPort->setPortName(portName);
    comPort->setBaudRate(baudrate);
    comPort->open(QIODevice::ReadWrite);
    comPort->setDataBits(QSerialPort::Data8);

    return comPort;
}



