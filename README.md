# Serial Monitor

Serial monitor to transmit and receive data over serial. Created on Qt C++.

<img src="/screenshots/screenshot1.png" width="645" height="381">



## Builds 

* [Windows 10 - 32bit/64bit](https://gitlab.com/karlvaello/serial-monitor/tree/master/builds/release-win32bit-v1.1.1) ![CI status](https://img.shields.io/badge/release-v1.1.1-blue.svg?longCache=true&style=flat)

* MacOS

* Linux

## Installation

Not requiered, download build, it contains all required files. Portable.

### Requirements
* Windows 10 - 32bit


## License ![CI status](https://img.shields.io/badge/license-GNU%20v3.0-green.svg?longCache=true&style=flat)
[GNU GENERAL PUBLIC LICENSE v3.0](https://gitlab.com/karlvaello/serial-monitor/blob/master/LICENSE)